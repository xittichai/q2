import { useEffect, useState } from 'react';
export default function App() {
    const [data, setData] = useState([]),
        [filterString, setFilterString] = useState('');
    useEffect(() =>
        fetch('https://api.publicapis.org/categories')
            .then(response => response.json())
            .then(data => setData(data))
    );
    return (
        <div className='App'>
            <input onChange={e => setFilterString(e.target.value)} placeholder='Search' type='text' />
            <div>
                {data
                    .filter(e => e.includes(filterString))
                    .map((e, i) => (
                        <p key={i}>{e}</p>
                    ))}
            </div>
        </div>
    );
}